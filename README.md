# Sigmoid COVID-19

Features: easy download of data from ECDC and the Brazilian Ministry of Health concerning the cases of infected and deceased individuals from COVID-19, fitting the data with sigmoid curves, analysis of convergence criteria and plots. 

===========

Getting started

1. Download the program, choosing between "tausigma.p" (MATLAB function) or "tausigma.zip" (stand-alone file) 

2. Run the program 

(a) "tausigma.p" may be run from MATLAB prompt, by typing "tausigma" (without quotes)

(b) unpack "tausigma.zip" and run the executable file from the operating system. Please be patient while the files are installed in the first run.

3. In the main menu, access "File: Download New Data File". The easiest way is to use the options to download the files directly, from the ECDC, Johns Hopkins University or brasil.io websites. 

4. There is a list of all available regions on the left of the main screen. Select a region from the list to see the data and perform the analysis. 

5. The edit field above the list may be used to look for a desired region.
